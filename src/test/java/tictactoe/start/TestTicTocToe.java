package tictactoe.start;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import tictactoe.pages.BasePage;
import tictactoe.pages.PageTicTacToe;

public class TestTicTocToe {
	@AfterClass
	public static void setUp() {
		BasePage.getInstance().close();
	}

	@Test
	public void testJogoRenderizado() {
		Assert.assertTrue("Pagina não foi renderizada", PageTicTacToe.getInstance().verificaJogoRenderizado());
	}

	@Test
	public void testXVenceNaPrimeiraLinhaCompleta() {
		try {
			PageTicTacToe.getInstance().XvenceNaPrimeiraLinha();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na primeira linha com X "+e);
		}
	}

	@Test
	public void testOVenceNaPrimeiraLinhaCompleta() {
		try {
			PageTicTacToe.getInstance().OvenceNaPrimeiraLinha();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na primeira linha com O "+e);
		}
	}

	@Test
	public void testXVenceNaSegundaLinhaCompleta() {
		try {
			PageTicTacToe.getInstance().XvenceNaSegundaLinha();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na segunda linha com X "+e);
		}
	}

	@Test
	public void testOVenceNaSegundaLinhaCompleta() {
		try {
			PageTicTacToe.getInstance().OvenceNaSegundaLinha();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na segunda linha com O "+e);
		}
	}


	@Test
	public void testXVenceNaTerceiraLinhaCompleta() {
		try {
			PageTicTacToe.getInstance().XvenceNaTerceiraLinha();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na terceira linha com X "+e);
		}
	}

	@Test
	public void testOVenceNaTerceiraLinhaCompleta() {
		try {
			PageTicTacToe.getInstance().OvenceNaTerceiraLinha();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na terceira linha com O "+e);
		}
	}

	@Test
	public void testXVenceNaPrimeiraColunaCompleta() {
		try {
			PageTicTacToe.getInstance().XvenceNaPrimeiraColuna();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na primeira coluna com X "+e);
		}
	}

	@Test
	public void testOVenceNaPrimeiraColunaCompleta() {
		try {
			PageTicTacToe.getInstance().OvenceNaPrimeiraColuna();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na primeira coluna com O "+e);
		}
	}

	@Test
	public void testXVenceNaSegudaColunaCompleta() {
		try {
			PageTicTacToe.getInstance().XvenceNaSegundaColuna();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na segunda coluna com X "+e);
		}
	}

	@Test
	public void testOVenceNaSegundaColunaCompleta() {
		try {
			PageTicTacToe.getInstance().OvenceNaSegundaColuna();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na segunda coluna com O "+e);
		}
	}

	@Test
	public void testXVenceNaTerceiraColunaCompleta() {
		try {
			PageTicTacToe.getInstance().XvenceNaTerceiraColuna();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na terceira coluna com X "+e);
		}
	}

	@Test
	public void testOVenceNaTerceiraColunaCompleta() {
		try {
			PageTicTacToe.getInstance().OvenceNaTerceiraColuna();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na terceira coluna com O "+e);
		}
	}

	@Test
	public void testXVenceNaDiagonalEsquerda() {
		try {
			PageTicTacToe.getInstance().XvenceNaDiagonalEsqueda();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na Diagonal Esqueda com X "+e);
		}
	}

	@Test
	public void testOVenceNaDiagonalEsquerda() {
		try {
			PageTicTacToe.getInstance().OvenceNaDiagonalEsqueda();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na Diagonal Esqueda com O "+e);
		}
	}

	@Test
	public void testXVenceNaDiagonalDireita() {
		try {
			PageTicTacToe.getInstance().XvenceNaDiagonalDireita();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na Diagonal direita com X "+e);
		}
	}

	@Test
	public void testOVenceNaDiagonalDireita() {
		try {
			PageTicTacToe.getInstance().OvenceNaDiagonalDireita();
		} catch (Exception e) {
			Assert.fail("ERRO! Não foi finalizada a jogada na Diagonal Direita com O "+e);
		}
	}

	@Test
	public void testSelecionaCelulaJaSelecionada() {
			Assert.assertTrue("Erro! Mesma celula selecionada duas vezes",PageTicTacToe.getInstance().selecionarCelulaJaSelecionada());
	}
	
	@Test
	public void testNewGame(){
			Assert.assertTrue("Erro! Jogo não esta reiniciando",PageTicTacToe.getInstance().verificaNewGame());
	}
}
