package tictactoe.framework;

/**
 * 
 * @author Hezio D. Fernandes - QA Automation Engineer
 *
 */

public class MessageConstants {

	public static final String URL = "Set here the path tic_tac_toe.html";
	
	//  Example
	//	public static final String URL = "file:////home/hezio/amb_dev/tic-tac-toe/tic_tac_toe- js/tic_tac_toe.html";

}
