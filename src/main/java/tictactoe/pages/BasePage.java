package tictactoe.pages;

import org.openqa.selenium.WebDriver;

import tictactoe.framework.MessageConstants;
import tictactoe.webdriver.ChromeWebDriver;

/**
 * 
 * @author Hezio D. Fernandes - QA Automation Engineer
 *
 */

public class BasePage {

	private static WebDriver driver;
	private static BasePage instance;

	public BasePage() {
		if (driver == null) {
			driver = ChromeWebDriver.createAndStartService();
			driver.manage().window().maximize();
			driver.get(MessageConstants.URL);
		}
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void close() {
		getDriver().close();
	}

	public static BasePage getInstance() {
		if (instance == null) {
			instance = new BasePage();
		}
		return instance;

	}

}