package tictactoe.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageTicTacToe extends BasePage {
	private static PageTicTacToe instance;

	@FindBy(id = "c11")
	private WebElement celula11;

	@FindBy(id = "c12")
	private WebElement celula12;

	@FindBy(id = "c13")
	private WebElement celula13;

	@FindBy(id = "c21")
	private WebElement celula21;

	@FindBy(id = "c22")
	private WebElement celula22;

	@FindBy(id = "c23")
	private WebElement celula23;

	@FindBy(id = "c31")
	private WebElement celula31;

	@FindBy(id = "c32")
	private WebElement celula32;

	@FindBy(id = "c33")
	private WebElement celula33;

	@FindBy(name = "clean")
	private WebElement newGame;

	public PageTicTacToe() {
		PageFactory.initElements(getDriver(), this);
	}

	public boolean verificaJogoRenderizado() {
		if (getDriver().getTitle().equalsIgnoreCase("Tic Tac Toe")) {
			return true;
		}
		return false;
	}

	public static PageTicTacToe getInstance() {
		if (instance == null) {
			instance = new PageTicTacToe();
		}
		return instance;
	}

	public void XvenceNaPrimeiraLinha() {
		celula11.click();
		celula21.click();
		celula12.click();
		celula31.click();
		celula13.click();
		getDriver().switchTo().alert().accept();
	}

	public void OvenceNaPrimeiraLinha() {
		celula21.click();
		celula11.click();
		celula31.click();
		celula12.click();
		celula33.click();
		celula13.click();
		getDriver().switchTo().alert().accept();
	}

	public void XvenceNaSegundaLinha() {
		celula21.click();
		celula11.click();
		celula22.click();
		celula12.click();
		celula23.click();
		getDriver().switchTo().alert().accept();
	}

	public void OvenceNaSegundaLinha() {
		celula11.click();
		celula21.click();
		celula12.click();
		celula22.click();
		celula33.click();
		celula23.click();
		getDriver().switchTo().alert().accept();
	}

	public void XvenceNaTerceiraLinha() {
		celula31.click();
		celula11.click();
		celula32.click();
		celula12.click();
		celula33.click();
		getDriver().switchTo().alert().accept();

	}

	public void OvenceNaTerceiraLinha() {
		celula11.click();
		celula31.click();
		celula12.click();
		celula32.click();
		celula22.click();
		celula33.click();
		getDriver().switchTo().alert().accept();
	}

	public void XvenceNaPrimeiraColuna() {
		celula11.click();
		celula33.click();
		celula21.click();
		celula13.click();
		celula31.click();
		getDriver().switchTo().alert().accept();
	}

	public void OvenceNaPrimeiraColuna() {
		celula33.click();
		celula11.click();
		celula13.click();
		celula21.click();
		celula22.click();
		celula31.click();
		getDriver().switchTo().alert().accept();

	}

	public void XvenceNaSegundaColuna() {
		celula12.click();
		celula33.click();
		celula22.click();
		celula13.click();
		celula32.click();
		getDriver().switchTo().alert().accept();
	}

	public void OvenceNaSegundaColuna() {
		celula33.click();
		celula12.click();
		celula13.click();
		celula22.click();
		celula21.click();
		celula32.click();
		getDriver().switchTo().alert().accept();

	}

	public void XvenceNaTerceiraColuna() {
		celula13.click();
		celula31.click();
		celula23.click();
		celula22.click();
		celula33.click();
		getDriver().switchTo().alert().accept();
	}

	public void OvenceNaTerceiraColuna() {
		celula31.click();
		celula13.click();
		celula22.click();
		celula23.click();
		celula11.click();
		celula33.click();
		getDriver().switchTo().alert().accept();

	}

	public void XvenceNaDiagonalDireita() {
		celula13.click();
		celula11.click();
		celula22.click();
		celula23.click();
		celula31.click();
		getDriver().switchTo().alert().accept();
	}

	public void OvenceNaDiagonalDireita() {
		celula11.click();
		celula13.click();
		celula23.click();
		celula22.click();
		celula12.click();
		celula31.click();
		getDriver().switchTo().alert().accept();

	}

	public void XvenceNaDiagonalEsqueda() {
		celula11.click();
		celula13.click();
		celula22.click();
		celula21.click();
		celula33.click();
		getDriver().switchTo().alert().accept();

	}

	public void OvenceNaDiagonalEsqueda() {
		celula13.click();
		celula11.click();
		celula31.click();
		celula22.click();
		celula21.click();
		celula33.click();
		getDriver().switchTo().alert().accept();

	}

	public boolean selecionarCelulaJaSelecionada() {
		try {
			celula13.click();
			celula11.click();
			celula11.click();
			celula22.click();
			celula21.click();
			celula33.click();
			return false;
		} catch (Exception e) {
			getDriver().switchTo().alert().accept();
			newGame.click();
			return true;
		}

	}

	public boolean verificaNewGame() {
		celula13.click();
		celula11.click();
		celula21.click();
		newGame.click();
		if (celula13.getText().equalsIgnoreCase("X")) {
			return false;
		}
		return true;
	}

}
