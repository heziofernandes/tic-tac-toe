moviment = "X";

function move(cell){

  var cellClicked = document.getElementById(cell).textContent;
  if (cellClicked == "X" || cellClicked == "O"){
    alert("Oops, this box has been selected!");
  }
  else{
    document.getElementById(cell).textContent = moviment;
    if (moviment == "X"){
      moviment = "O";
    }else{
      moviment = "X";
    }
    getWinner();
  }
}

function getWinner(){
  var ce11 = document.getElementById('c11').textContent;
  var ce12 = document.getElementById('c12').textContent;
  var ce13 = document.getElementById('c13').textContent;
  var ce21 = document.getElementById('c21').textContent;
  var ce22 = document.getElementById('c22').textContent
  var ce23 = document.getElementById('c23').textContent;
  var ce31 = document.getElementById('c31').textContent;
  var ce32 = document.getElementById('c32').textContent;
  var ce33 = document.getElementById('c33').textContent;

  if (((ce11 !== '') && (ce12 !== '') && (ce13 !== '') && (ce11 == ce12) && (ce12 == ce13)) ||
    ((ce11 !== '') && (ce22 !== '') && (ce33 !== '') && (ce11 == ce22) && (ce22 == ce33)) ||
    ((ce11 !== '') && (ce21 !== '') && (ce31 !== '') && (ce11 == ce21) && (ce21 == ce31)) ||
    ((ce21 !== '') && (ce22 !== '') && (ce23 !== '') && (ce21 == ce22) && (ce22 == ce23)) ||
    ((ce31 !== '') && (ce32 !== '') && (ce33 !== '') && (ce31 == ce32) && (ce32 == ce33)) ||
    ((ce12 !== '') && (ce22 !== '') && (ce32 !== '') && (ce12 == ce22) && (ce22 == ce32)) ||
    ((ce13 !== '') && (ce23 !== '') && (ce33 !== '') && (ce13 == ce23) && (ce23 == ce33)) ||
    ((ce31 !== '') && (ce22 !== '') && (ce13 !== '') && (ce31 == ce22) && (ce22 == ce13))){
	  if(moviment == "X"){
		  alert("The winner is 'O'");
	    }else{
	    	alert("The winner is 'X'");
	  }
      playAgain();
    }
  else {
    if (ce11 != '' && ce12 != '' && ce13 != '' &&
      ce21 != '' && ce22 != '' && ce23 != '' &&
      ce31 != '' && ce32 != '' && ce33 != ''){
        alert("There wasn't winner");
        playAgain();
    }
  }
}

function playAgain(){
  for (var i=1; i<=3; i++){
    for (var j=1; j<=3; j++){
      celName = 'c' + i + j
      document.getElementById(celName).textContent = '';
    }
  }
  moviment = "X";
}
